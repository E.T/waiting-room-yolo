#!/bin/bash

while [ 1=1 ]
do
  cd ~/code/darknet
  rm -f ~/code/darknet/data/room.jpg
  sleep 1

  rsync -avz -e ssh cam@10.131.91.78:photos/*.jpg ~/code/darknet/data/

  var=$(exiftool -fileinodechangedate /home/linux/code/darknet/data/room.jpg)

  ./darknet detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.35 -dont_show -ext_output data/room.jpg | grep person | sed 's/\%.*/%/' > /home/linux/code/result.txt
  #./darknet detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.20 -dont_show -ext_output data/room.jpg | sed 's/\%.*/%/' >> /home/linux/code/result.txt

  echo Updated > ~/code/updatetime.html
  echo ${var#*:} >> ~/code/updatetime.html

  echo -e "<link href="txtstyle.css" rel="stylesheet" type="text/css" />\n" > ~/code/total.html
  echo '<h1 style="font-size:900%;text-align:center;">' >> ~/code/total.html
  grep -o 'person' ~/code/result.txt | wc -l >> ~/code/total.html
  echo "</h1>" >> ~/code/total.html

  echo -e "<link href="txtstyle.css" rel="stylesheet" type="text/css" />\n" > ~/code/result.html
  echo '<h1>' >> ~/code/result.html
  grep -o 'person' ~/code/result.txt >> ~/code/result.html
  echo "</h1>" >> ~/code/result.html

  echo done;sleep 5

done
